### @@n@@ Repository

This is a demo repository for `helloworld`
  
#### To Install @@n@@ Repository

1. Debian based (apt)

```bash
curl -sS @@u@@/repo.deb.sh | sudo bash
```

2. RPM based

```bash
curl -sS @@u@@/repo.rpm.sh | sudo bash
```

#### to install package from @@n@@

1. Debian based (apt)

```bash
apt-get install <package_name>
```

2. RPM based

```bash
yum install <package_name>
```

